#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, getPredictedRating, getActualRating
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3365\n3.2405\n3.5835\n0.84\n")

    def test_eval_2(self):
        r = StringIO("9731:\n255836\n893778\n1484395\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "9731:\n3.7344999999999997\n3.48\n3.3594999999999997\n0.37\n")


    def test_getPredictedRating_1(self):
        movieID = "10040"
        userID = "2417853"
        pred = getPredictedRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)
    def test_getPredictedRating_2(self):
        movieID = "6581"
        userID = "2324471"
        pred = getPredictedRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)
    def test_getPredictedRating_3(self):
        movieID = "11941"
        userID = "710933"
        pred = getPredictedRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)
    def test_getPredictedRating_4(self):
        movieID = "10004"
        userID = "735147"
        pred = getPredictedRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)

    def test_getActualRating_1(self):
        movieID = "10040"
        userID = "2417853"
        pred = getActualRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)
    def test_getActualRating_2(self):
        movieID = "6581"
        userID = "2324471"
        pred = getActualRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)
    def test_getActualRating_3(self):
        movieID = "11941"
        userID = "710933"
        pred = getActualRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)
    def test_getActualRating_3(self):
        movieID = "10004"
        userID = "735147"
        pred = getActualRating(userID,movieID)
        self.assertTrue(0 <= pred <= 5)









# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
