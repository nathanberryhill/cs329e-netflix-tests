#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, rating_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------


class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10040:\n3.3\n3.2\n3.6\n0.83\n')

    def test_eval_2(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '1:\n3.7\n3.5\n3.6\n0.48\n')

    def test_eval_3(self):
        r = StringIO("1000:\n2326571\n977808\n1010534\n1861759")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(w.getvalue(), '1000:\n3.4\n3.3\n3.1\n4.1\n0.75\n')

    # ----
    # rating_prediction
    # ----

    def test_rating_prediction_1(self):
        self.assertEqual(rating_prediction(30878, 1), 3.7)

    def test_rating_prediction_2(self):
        self.assertEqual(rating_prediction(2647871, 1), 3.5)

    def test_rating_prediction_3(self):
        self.assertEqual(rating_prediction(1283744, 1), 3.6)


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
